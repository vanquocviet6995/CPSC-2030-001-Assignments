<?php

require_once('server/router.php');
require_once('classes/message.php');

//Initalizing the router class
$app = new Router();


$app->get('/', function($request, $response) {
	$response->sendFile('views/index.html');
});


$app->post('/api/messages', function($request, $response) {

	if(empty($request["body"]["user"]) || empty($request["body"]["text"]))
		return $response->json(["error" => true], 400);

	$message = new Message();

	$result = $message->add($request["body"]);

	$response->json($result);
});

$app->get('/api/messages', function($request, $response) {
	$date = $request["params"]["date"] ?? strtotime('-1 hour');

	$message = new Message();

	$response->json($message->fetch($date));
});


//Error Handler
$app->error(function(Exception $e, $response){
  $response->send('Not found',404);
});


//Starting the router
$app->start();


?>

