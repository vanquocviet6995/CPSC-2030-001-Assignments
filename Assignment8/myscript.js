var flag = false;
var top = flag ? 0:-30; //menu top
var left = flag ? 0:-10; //menu left
var deg = flag ? 0:30; //menu rotate
$("#menu").click(function(){
	if (flag == true) {
		$(this).css({
			"transform":"rotate(-0deg)",
			"bottom":"21px",
			"left":"0px",
			"-moz-transform":"rotate(-0deg)",
			"-moz-transition" : "all .4s",
			"-webkit-transform":"rotate(-0deg)",
			"-ms-transform":"rotate(-0deg)",
			"-ms-transition" : "all .4s",
			"transform":"rotate(-0deg)",
			"transition":"all .4s"
		});

		$("#symbol").css({
			"bottom":"40px",
			"left":"86px",
			"transition": "all 0.3s ease-in-out"
		});

		$("#i1").css({
			"transform":"translate(0px,0px)",
			"transition": "all 1s ease-in-out"
		});

		$("#i2").css({
			"transform":"translate(0px,0px)",
			"transition": "all 1s ease-in-out"
		});

		$("#i3").css({
			"transform":"translate(0px,0px)",
			"transition": "all 1s ease-in-out"
		});

		$("#i4").css({
			"transform":"translate(0px,0px)",
			"transition": "all 1s ease-in-out"
		});

		$("#i5").css({
			"transform":"translate(0px,0px)",
			"transition": "all 1s ease-in-out"
		});
		flag = false;
	}
	else {
		$(this).css({
			"transform":"rotate(-"+ deg +"deg)",
			"bottom":"47px",
			"left":"-2px",
			"-moz-transform":"rotate(-30deg)",
			"-moz-transition" : "all .4s",
			"-webkit-transform":"rotate(-30deg)",
			"-ms-transform":"rotate(-30deg)",
			"-ms-transition" : "all .4s",
			"transform":"rotate(-30deg)",
			"transition":"all .4s"
		});

		$("#symbol").css({
			"bottom":"90px",
			"left":"86px",
			"transition": "all 0.3s ease-in-out"
		});

		$("#i1").css({
			"transform":"translate(200px,0px)",
			"transition": "all 1s ease-in-out"
		});

		$("#i2").css({
			"transform":"translate(300px,0px)",
			"transition": "all 1s ease-in-out"
		});

		$("#i3").css({
			"transform":"translate(400px,0px)",
			"transition": "all 1s ease-in-out"
		});

		$("#i4").css({
			"transform":"translate(500px,0px)",
			"transition": "all 1s ease-in-out"
		});

		$("#i5").css({
			"transform":"translate(600px,0px)",
			"transition": "all 1s ease-in-out"
		});
		flag = true;
	}
});

$("#i1").click(function(){
  $("#symbol").removeClass();
  $("#symbol").css({
	  "animation-name":"path",
	  "animation-timing-function": "linear",
	  "animation-duration": "2s"
  });
});

$("#i2").click(function(){
  $("#symbol").removeClass();
  $("#symbol").css({
	  "animation-name":"path2",
	  "animation-timing-function": "linear",
	  "animation-duration": "2s"
  });
});

$("#i3").click(function(){
  $("#symbol").removeClass();
  $("#symbol").css({
	  "animation-name":"path3",
	  "animation-timing-function": "linear",
	  "animation-duration": "2s"
  });
});

$("#i4").click(function(){
  $("#symbol").removeClass();
  $("#symbol").css({
	  "animation-name":"path4",
	  "animation-timing-function": "linear",
	  "animation-duration": "2s"
  });
});

$("#i5").click(function(){
  $("#symbol").removeClass();
  $("#symbol").css({
	  "animation-name":"path5",
	  "animation-timing-function": "linear",
	  "animation-duration": "2s"
  });
});