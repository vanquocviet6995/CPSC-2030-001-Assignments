(function($, window, undefined) {

	var username;
	var lastMessage = null;

	function addMessage(message) {

		var chat = $('#chat');

		var html = "";
		html += "<div class='bubble '>";
		html += "	<div class='user '>" + message.user + "</div>";
		html += "	<div class='message '>";
		html += "		<p class='text '>" + message.text + "</p>";
		html += "		<span class='time '>" + message.time + "</span>";
		html += "	</div>";
		html += "</div>";

		if(!lastMessage || lastMessage < message.date)
			lastMessage = message.date;

		chat.append(html);

		chat.scrollTop(function() {
			return this.scrollHeight;
		});
	}


	function login() {

		username = $('#username').val();

		startChat();
	}

	function fetchMessages() {

		console.log('Fetching messages since', lastMessage);
		$.ajax({
			url: '/api/messages?date=' + lastMessage,
			success: function(messages) {
				messages.forEach(addMessage);
			}
		});
	}

	function startChat() {

		if (!username.trim())
			return;

		$('#login-wrapper').hide();
		$('#wrapper').addClass('active');

		$('#send').on('click', sendMessage);

		fetchMessages();

		setInterval(fetchMessages, 1000 * 10);

	}

	function sendMessage() {

		var message = $("#message").val().trim();

		if(!message)
			return false;

		$("#message").val('');

		$.ajax(settings = {
			url: '/api/messages',
			method: 'POST',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
				accept: 'application/json'
			},
			data: {
				text: message,
				user: username
			},
			success: function(message) {
				addMessage(message);
			}
		})

	}

	$(document).ready(function() {
		$("#login").on('click', login)

		$(document).on('keydown', 'input', function(e) {
			if(e.which === 13) { // enter
				$(this).siblings('button').trigger('click');
			}
		});
	});



})(jQuery, window);