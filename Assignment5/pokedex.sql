DELIMITER //
CREATE PROCEDURE GetPoker(IN PokerType VARCHAR(255))
 BEGIN
 IF PokerType='All' THEN
	SELECT * FROM pokemon;
 ELSE
	SELECT * FROM pokemon WHERE Type=PokerType;
 END IF;
 END //
DELIMITER ;
DELIMITER //
CREATE PROCEDURE GetMega(IN pokedex_entry_p INT)
 BEGIN
 SELECT * FROM MEGA WHERE pokedex_entry=pokedex_entry_p;
 END //
DELIMITER ;
DELIMITER //
CREATE PROCEDURE GetAllTypes()
 BEGIN
 select DISTINCT type from pokemon;
 END //
DELIMITER ;