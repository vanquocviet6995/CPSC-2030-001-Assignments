<!DOCTYPE html>
<html lang="en">
 {% include('head.twig.html') with {'title':title} %}

<body style="height: 100%;">
{% include('top.twig.html') %}
{% include('nav.twig.php') %}


 <?php
session_start();

if(!isset($_SESSION['user']))
{
  header("Location: index.php");
}
if(isset($_GET['logout']))
{
  session_destroy();
  unset($_SESSION['user']);
  header("Location: index.php");
}

?>
 

<!-- END OF NAVIGATION -->



	<!-- Contact -->

	<div class="contact">
		<div class="contact_info_container">
			<div class="container">
				<div class="row">
					<div class="col-lg-6">
						<div class="contact_form">
							<div class="contact_info_title">Contact Form</div>
							<form action="#" class="comment_form">
								<div>
									<div class="form_title">Name</div>
									<input type="text" class="comment_input" name="name" required="required">
								</div>
								<div>
									<div class="form_title">Email</div>
									<input type="text" class="comment_input" name="email" required="required">
								</div>
								<div>
									<div class="form_title">Message</div>
									<textarea class="comment_input comment_textarea" name="message" required="required"></textarea>
								</div>
								<div>
									<button type="submit" class="comment_button trans_200">submit now</button>
								</div>
							</form>
						</div>
					</div>

					<div class="col-lg-6">
						<div class="contact_info">
							
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- END OF CONTACT -->
<?php

if( isset($_POST['Submit']) ) {	 

$subject = "Subject";

$message=$_POST["message"];

$name= $_POST["name"];

$mail_from=$_POST["email"]; 

$header= "from: $name $mail_from";

$to = 'email@gmail.com';

$send_contact= mail($to,$subject,$message,$header);
}
if($send_contact){
echo "Email Receive";
}
else {
echo "ERROR";
}

?>

	<!-- FOOTER -->

  {% include('footer.twig.html')%}
	
<!-- END OF FOOTER -->

</body>
</html>