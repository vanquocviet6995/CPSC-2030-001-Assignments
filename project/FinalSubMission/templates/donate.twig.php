<!DOCTYPE html>
<html style="height: 100%;">
 {% include('head.twig.html') with {'title':title} %}



<body style="height: 100%;">
{% include('top.twig.html') %}
{% include('nav.twig.php') %}

 <?php
session_start();

if(!isset($_SESSION['user']))
{
  header("Location: index.php");
}
if(isset($_GET['logout']))
{
  session_destroy();
  unset($_SESSION['user']);
  header("Location: index.php");
}

?>
 
    <!-- NAVIGATION -->
              <div class="containerdark">
                <div class="wrapper" id="top_div">
                  <div id="top_left">
    
  
             
              </div>
      
      
          </div>






<div class="row">
  <div class="col-sm-6">
    <img src="images/donate.jpg" alt="" >
    
  </div>
  

<div class="col-sm-6" id="donatetext">
  <h2>How you can help</h2><br>
<h4>Thanks to donations from individuals, companies or governments the Children and War Foundation is able to help children after wars and disasters. </h4><br>


<div>

<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
 
    <input type="hidden" name="cmd" value="_xclick">
    <input type="hidden" name="business" value="accounts@freelanceswitch.com">
    <strong >Donation/Cont</strong><br />
    
    <select name="item_name">
      <option value="Donation">Donation</option>
      <option value="Contribution">Contribution</option>
    </select>
 <br><br>
    <strong>Pay with Paypal</strong><br />   
    
    <select name="item_number">
      <option value="PayPal Form Tutorial">PayPal</option>
      <option value="Some Other Tutorial">Some Other way</option>
    </select>
 <br><br>
    <strong>Select Amount</strong><br />
    $ <input type="text" name="amount">
 
    <input type="hidden" name="no_shipping" value="0">
    <input type="hidden" name="no_note" value="1">
    <input type="hidden" name="currency_code" value="USD">
    <input type="hidden" name="lc" value="AU">
    <input type="hidden" name="bn" value="PP-BuyNowBF">
    <input type="hidden" name="return" value="http://net.tutsplus.com/payment-complete/">
 
    <br/><br/>
    <input type="submit" class="btn btn-success" value="Donate Now!">
 
</form>

<br/>



</div>
</div>
  </div> 
  </div>

  {% include('footer.twig.html')%}
</body>
</html> 