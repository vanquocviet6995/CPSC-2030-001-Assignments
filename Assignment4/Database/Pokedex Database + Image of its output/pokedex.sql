/* New Database name Pkedex */
CREATE SCHEMA pokedex;
USE pokedex;

CREATE TABLE `pokemon` (
  `Nat` int(11) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `Type` varchar(20) NOT NULL,
  `HP` int(11) NOT NULL,
  `Atk` int(11) NOT NULL,
  `DEF` int(11) NOT NULL,
  `SAt` int(11) NOT NULL,
  `SDf` int(11) NOT NULL,
  `Spd` int(11) NOT NULL,
  `BST` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE strength (
    strong VARCHAR(20),
    vulnerable VARCHAR(20) )
     ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE resistance (
    resistant VARCHAR(20) NOT NULL,
    weak VARCHAR(20) NOT NULL )
 ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE form (
    img_url VARCHAR(100),
    form_id INT,
    pokedex_entry INT NOT NULL,
    name VARCHAR(40) NOT NULL,
    type1 VARCHAR(20) NOT NULL,
    type2 VARCHAR(20),
    hp INT NOT NULL,
    atk INT NOT NULL,
    def INT NOT NULL,
    sat INT NOT NULL,
    sdf INT NOT NULL,
    spd INT NOT NULL,
    bst INT NOT NULL)
 ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `pokemonvulnerable` (
  `Type` varchar(140) NOT NULL,
  `Vulnerable To` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE type (
    type VARCHAR(20),
    PRIMARY KEY (type)
);

CREATE TABLE mega (
    img_url VARCHAR(100),
    pokedex_entry INT,
    name VARCHAR(40) NOT NULL,
    type VARCHAR(20) NOT NULL,
    hp INT NOT NULL,
    atk INT NOT NULL,
    def INT NOT NULL,
    sat INT NOT NULL,
    sdf INT NOT NULL,
    spd INT NOT NULL,
    bst INT NOT NULL,
    Strong VARCHAR(20) NOT NULL,
    Weak VARCHAR(20) NOT NULL,
    Vulnerable VARCHAR(20) NOT NULL,
    Resistant VARCHAR(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



INSERT INTO mega VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/323/447/215/199/42901.png', 428, 'Mega Lopunny', 'NORMAL', 65, 136, 94, 54, 96, 135, 580, 'Normal Dark', 'Flying', 'Rock', 'Psychic');
INSERT INTO mega VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/323/434/215/199/10058.png', 445, 'Mega Garchomp', 'DRAGON', 108, 170, 115, 120, 95, 92, 700, 'Fighting', 'Steel', 'Ground', 'Rock');
INSERT INTO mega VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/323/424/215/199/100076.png', 448, 'Mega Lucario', 'FIGHTING', 70, 145, 88, 140, 70, 112, 625, 'Poison', 'Grass', 'Steel', 'Ground');
INSERT INTO mega VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/323/435/215/199/10060.png', 460, 'Mega Abomasnow', 'GRASS', 90, 132, 105, 132, 105, 30, 594, 'Poison', 'Grass', 'Ground', 'Steel');
INSERT INTO mega VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/503/215/199/635532020063317332.png', 475, 'Mega Gallade', 'PSYCHIC',68, 165, 95, 65, 115, 110, 618, 'Dark', 'Rock', 'Normal', 'Ground');
INSERT INTO mega VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/323/455/215/199/53201.png', 498, 'Tepig', 'FIRE', 103, 60, 126, 80, 126, 50, 545, 'Fighting', 'Grass', 'FIRE', 'Dark');
INSERT INTO mega VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/323/455/215/199/53201.png', 499, 'Pignite', 'FIRE', 103, 61, 126, 80, 126, 50, 545, 'Fighting', 'Grass', 'FIRE', 'Dark');



INSERT INTO `pokemon` (`Nat`, `Name`, `Type`, `HP`, `Atk`, `DEF`, `SAt`, `SDf`, `Spd`, `BST`) VALUES
(387, 'Turtwig', 'GRASS', 55, 68, 64, 45, 55, 31, 318),
(388, 'Grotle', 'GRASS', 75, 89, 85, 55, 65, 36, 405),
(389, 'Torterra', 'GRASS, GROUND', 95, 109, 105, 75, 85, 56, 525),
(390, 'Chimchar', 'FIRE', 44, 58, 44, 58, 44, 61, 309),
(391, 'Monferno', 'FIRE, FIGHT', 64, 78, 52, 78, 52, 81, 405),
(392, 'Infernape', 'FIRE, FIGHT', 76, 104, 71, 104, 71, 108, 534),
(393, 'Piplup', 'WATER', 53, 51, 53, 61, 56, 40, 314),
(394, 'Prinplup', 'WATER', 64, 66, 68, 81, 76, 50, 405),
(395, 'Empoleon', 'WATER, STEEL', 84, 86, 88, 111, 101, 60, 530),
(396, 'Starly', 'NORMAL, FLYING', 40, 55, 30, 30, 30, 60, 245),
(397, 'Staravia', 'NORMAL, FLYING', 55, 75, 50, 40, 40, 80, 340),
(398, 'Staraptor', 'NORMAL, FLYING', 85, 120, 70, 50, 60, 100, 485),
(399, 'Bidoof', 'NORMAL', 59, 45, 40, 35, 40, 31, 250),
(400, 'Bibarel', 'NORMAL , WATER', 79, 85, 60, 55, 60, 71, 410),
(401, 'Kricketot', 'BUG', 37, 25, 41, 25, 41, 25, 194),
(402, 'Kricketune', 'BUG', 77, 85, 51, 55, 51, 65, 384),
(403, 'Shinx', 'ELECTRIC', 45, 65, 34, 40, 34, 45, 263),
(404, 'Luxio', 'ELECTRIC', 60, 85, 49, 60, 49, 60, 363),
(405, 'Luxray', 'ELECTRIC', 80, 120, 79, 95, 79, 70, 523),
(406, 'Budew', 'GRASS , POISION', 40, 30, 35, 50, 70, 55, 280),
(407, 'Roserade', 'GRASS , POISION', 60, 70, 65, 125, 105, 90, 515),
(408, 'Cranidos', 'ROCK', 67, 125, 40, 30, 30, 58, 350),
(409, 'Rampardos', 'ROCK', 97, 165, 60, 65, 50, 58, 495),
(410, 'Shieldon', 'ROCK, STEEL', 30, 42, 118, 42, 88, 30, 350),
(411, 'Bastiodon', 'ROCK, STEEL', 60, 52, 168, 47, 138, 30, 495),
(412, 'Burmy', 'BUG', 40, 29, 45, 29, 45, 36, 224),
(414, 'Mothim', 'BUG, FLYING', 70, 94, 50, 94, 50, 66, 424),
(415, 'Combee', 'BUG, FLYING', 30, 30, 42, 30, 42, 70, 244),
(416, 'Vespiquen', 'BUG, FLYING', 70, 80, 102, 80, 102, 40, 474),
(417, 'Pachirisu', 'ELECTRIC', 60, 45, 70, 45, 90, 95, 405),
(418, 'Buizel', 'WATER', 55, 65, 35, 60, 30, 85, 330),
(419, 'Floatzel', 'WATER', 85, 105, 55, 85, 50, 115, 495),
(420, 'Cherubi', 'GRASS', 45, 35, 45, 62, 53, 35, 275),
(421, 'Cherrim', 'GRASS', 70, 60, 70, 87, 78, 85, 450),
(421, 'Cherrim', 'GRASS', 70, 60, 70, 87, 78, 85, 450),
(422, 'Shellos', 'WATER', 76, 48, 48, 57, 62, 34, 325),
(423, 'Gastrodon', 'WATER, GROUND', 111, 83, 68, 92, 82, 39, 475),
(424, 'Ambipom', 'NORMAL', 75, 100, 66, 60, 66, 115, 482),
(425, 'Drifloon', 'GHOST, FLYING', 90, 50, 34, 60, 44, 70, 348),
(426, 'Drifblim', 'GHOST, FLYING', 150, 80, 44, 90, 54, 80, 498),
(427, 'Buneary', 'NORMAL', 55, 66, 44, 44, 85, 85, 350),
(428, 'Lopunny', 'NORMAL', 65, 76, 84, 54, 96, 105, 480),
(428, 'Mega Lopunny', 'NORMAL, FIGHT', 65, 136, 94, 54, 96, 135, 580),
(429, 'Mismagius', 'GHOST', 60, 60, 60, 105, 105, 105, 495),
(430, 'Honchkrow', 'DARK, FLYING', 100, 125, 52, 105, 52, 71, 505),
(431, 'Glameow', 'NORMAL', 49, 55, 42, 42, 37, 85, 310),
(432, 'Purugly', 'NORMAL', 71, 82, 64, 64, 59, 112, 452),
(433, 'Chingling', 'PHYCHC', 45, 30, 50, 65, 50, 45, 285),
(433, 'Chingling', 'PHYCHIC', 45, 30, 50, 65, 50, 45, 285),
(434, 'Stunky', 'POISON, DARK', 63, 63, 47, 41, 41, 74, 329),
(435, 'Skuntank', 'POISON, DARK', 103, 93, 67, 71, 61, 84, 479),
(436, 'Bronzor', 'STEEL, PHYCHIC', 57, 24, 86, 24, 86, 23, 300),
(437, 'Bronzong', 'STEEL, PHYCHIC', 67, 89, 116, 79, 116, 33, 500),
(438, 'Bonsly', 'ROCK', 50, 80, 95, 10, 45, 10, 290),
(439, 'Mine Jr.', 'PSYCHIC, FAIRY', 20, 25, 45, 70, 90, 60, 310),
(440, 'Happiny', 'NORMAL', 100, 5, 5, 15, 65, 30, 220),
(441, 'Chatot', 'NORMAL, FLYING', 76, 65, 45, 92, 42, 91, 411),
(413, 'Wormadam', 'BUG, GRASS', 60, 59, 85, 79, 105, 36, 424),
(413, 'Wormadam(Sandy Cloak', 'BUG, GROUND', 60, 79, 105, 59, 85, 36, 424),
(413, 'Wormadam(Trash Cloak', 'BUG, STEEL', 60, 69, 95, 69, 95, 36, 424),
(442, 'Spiritomb', 'GHOST, DARK', 50, 92, 108, 92, 108, 35, 485),
(443, 'Gible', 'DRAGON, GROUND', 58, 70, 45, 40, 45, 42, 300),
(444, 'Gabite', 'DRAGON, GROUND', 68, 90, 65, 50, 55, 82, 410),
(445, 'Garchomp', 'DRAGON, GROUND', 108, 130, 95, 80, 85, 102, 600),
(445, 'Mega Garchomp', 'DRAGON, GROUND', 108, 170, 115, 120, 95, 92, 700),
(446, 'Munchlax', 'NORMAL', 135, 85, 40, 40, 85, 5, 390),
(447, 'Riolu', 'FIGHT', 40, 70, 40, 35, 40, 60, 285),
(448, 'Lucario', 'FIGHT,  STEEL', 70, 110, 70, 115, 70, 90, 525),
(448, 'Mega Lucario', 'FIGHT, STEEL', 70, 145, 88, 140, 70, 90, 525),
(449, 'Hippopotas', 'GROUND', 68, 72, 78, 38, 42, 32, 330),
(450, 'Hippowdon', 'GROUND', 108, 112, 118, 68, 72, 47, 525),
(451, 'Skorupi', 'POSION, BUG', 40, 50, 90, 30, 55, 65, 330),
(452, 'Drapion', 'POSION, DARK', 70, 90, 110, 60, 75, 95, 500),
(453, 'Croagunk', 'POSION, FIGHT', 48, 61, 40, 61, 40, 50, 300),
(454, 'Toxicroak', 'POISON, FIGHT', 83, 106, 65, 86, 65, 85, 490),
(455, 'Carnivine', 'GRASS', 74, 100, 72, 90, 72, 46, 454),
(456, 'Finneon', 'WATER', 49, 49, 56, 49, 61, 66, 330),
(457, 'Lumineon', 'WATER', 69, 69, 76, 69, 86, 91, 460),
(458, 'Mantyke', 'WATER, FLYING', 45, 20, 50, 60, 120, 50, 345),
(459, 'Snover', 'GRASS, ICE', 60, 62, 50, 62, 60, 40, 334),
(460, 'Abomasnow', 'GRASS, ICE', 90, 92, 75, 92, 85, 60, 494),
(460, 'Mega Abomasnow', 'GRASS, ICE', 90, 132, 105, 132, 105, 30, 594),
(461, 'Weavile', 'DARK, ICE', 70, 120, 65, 45, 85, 125, 510),
(462, 'Magnezone', 'ELECTRIC, STEEL', 70, 70, 115, 130, 90, 60, 535),
(463, 'Lickilicky', 'NORMAL', 110, 85, 95, 80, 95, 50, 515),
(464, 'Rhyperior', 'GROUND, ROCK', 115, 140, 130, 55, 55, 40, 535),
(465, 'Tangrowth', 'GRASS', 100, 100, 125, 110, 50, 50, 535),
(466, 'Electivire', 'ELECTRIC', 75, 123, 67, 95, 85, 95, 540),
(467, 'Magmortar', 'FIRE', 75, 95, 67, 125, 95, 83, 540),
(468, 'Togekiss', 'FAIRY, FLYING', 85, 50, 95, 120, 115, 80, 545),
(469, 'Yanmega', 'BUG, FLYING', 86, 76, 86, 116, 56, 95, 515),
(470, 'Leafeon', 'GRASS', 65, 110, 130, 60, 65, 95, 525),
(471, 'Glaceon', 'ICE', 65, 60, 110, 130, 95, 65, 525),
(472, 'Gliscor', 'GROUND, FLYING', 75, 95, 125, 45, 75, 95, 510),
(473, 'Manoswine', 'ICE, GROUND', 110, 130, 80, 70, 60, 80, 530),
(474, 'Porygon-Z', 'NORMAL', 85, 80, 70, 135, 75, 90, 535),
(475, 'Gallade', 'PSYCHIC, FIGHT', 68, 125, 65, 65, 115, 110, 618),
(475, 'Mega Gallade', 'PSYCHIC, FIGHT', 68, 165, 95, 65, 115, 110, 618),
(476, 'Probopass', 'ROCK, STEEL', 60, 55, 145, 75, 150, 110, 618),
(477, 'Dusknoir', 'GHOST', 45, 100, 135, 65, 135, 45, 525),
(478, 'Froslass', 'ICE, GHOST', 70, 80, 70, 80, 70, 110, 480),
(479, 'Rotom', 'ELECTRIC, GHOST', 50, 50, 77, 95, 77, 91, 440),
(479, 'Rotom(Heat Rotom)', 'ELECTRIC, FIRE', 50, 65, 107, 105, 107, 86, 520),
(479, 'Rotom(Wash Rotom)', 'ELECTRIC, WATER', 50, 65, 107, 105, 107, 86, 520),
(479, 'Rotom(Frost Rotom)', 'ELECTRIC, ICE', 50, 65, 107, 105, 107, 86, 520),
(479, 'Rotom(Fan Rotom)', 'ELECTRIC, FLYING', 50, 65, 107, 105, 107, 86, 520),
(479, 'Rotom(Mow Rotom)', 'ELECTRIC, GRASS', 50, 65, 107, 105, 107, 86, 520),
(480, 'Uxie', 'PSYCHIC', 75, 75, 130, 75, 130, 95, 580),
(481, 'Mesprit', 'PSYCHIC', 80, 105, 105, 105, 105, 80, 580),
(482, 'Azelf', 'PSYCHIC', 75, 125, 70, 125, 70, 115, 580),
(483, 'Dialga', 'STEEL, DRAGON', 100, 120, 120, 150, 100, 90, 680),
(484, 'Palkia', 'WATER, DRAGON', 90, 120, 100, 150, 120, 100, 680),
(485, 'Heatran', 'Fire, STEEL', 91, 90, 106, 130, 106, 77, 600),
(486, 'Regigigas', 'NORMAL', 110, 160, 110, 80, 110, 100, 670),
(487, 'Giratina', 'GHOST, DRAGON', 150, 100, 120, 100, 120, 90, 680),
(487, 'Giratina(Origin Form', 'GHOST, DRAGON', 150, 120, 100, 120, 100, 90, 680),
(488, 'Cresselia', 'PSYCHIC', 120, 70, 120, 75, 130, 85, 600),
(489, 'Phione', 'water', 80, 80, 80, 80, 80, 80, 480),
(490, 'Manaphy', 'water', 100, 100, 100, 100, 100, 100, 600),
(491, 'Darkrai', 'DARK', 70, 90, 90, 135, 90, 125, 600),
(492, 'Shaymin', 'GRASS', 100, 100, 100, 100, 100, 100, 600),
(492, 'Shaymin(Sky Forme)', 'GRASS, FLYING', 100, 103, 75, 120, 75, 127, 600),
(493, 'Arceus', 'NORMAL', 120, 120, 120, 120, 120, 120, 720),
(494, 'Victini', 'PSYCHIC, FIRE', 100, 100, 100, 100, 100, 100, 600),
(495, 'Snivy', 'GRASS', 45, 45, 55, 45, 55, 63, 308),
(496, 'Servine', 'GRASS', 60, 60, 75, 60, 75, 83, 413),
(497, 'Serperior', 'GRASS', 75, 75, 95, 75, 95, 113, 528),
(498, 'Tepig', 'FIRE', 65, 63, 45, 45, 45, 45, 308),
(499, 'Pignite', 'FIRE, FIGHT', 90, 93, 55, 70, 55, 55, 418),
(500, 'Emboar', 'FIRE, FIGHT', 110, 123, 65, 100, 65, 65, 528),
(501, 'Oshawott', 'WATER', 55, 55, 45, 63, 45, 45, 308),
(502, 'Dewott', 'WATER', 75, 75, 60, 83, 60, 60, 413),
(503, 'Samurott', 'WATER', 95, 100, 85, 108, 70, 70, 528),
(505, 'Watchog', 'NORMAL', 60, 85, 69, 60, 69, 77, 420),
(506, 'Lillipup', 'NORMAL', 45, 60, 45, 25, 45, 55, 275),
(507, 'Herdier', 'NORMAL', 65, 80, 65, 35, 45, 55, 275),
(508, 'Stoutland', 'NORMAL', 85, 110, 90, 45, 90, 80, 500),
(509, 'Purrloin', 'DARK', 41, 50, 37, 50, 37, 66, 281),
(510, 'Liepard', 'DARK', 64, 88, 50, 88, 50, 106, 446),
(511, 'Pansage', 'GRASS', 50, 53, 48, 53, 48, 64, 316),
(512, 'Simisage', 'GRASS', 75, 98, 63, 98, 63, 101, 498),
(513, 'Pansear', 'FIRE', 50, 53, 48, 53, 48, 64, 316),
(514, 'Simisear', 'FIRE', 75, 98, 63, 98, 63, 101, 498),
(515, 'Panpour', 'WATER', 50, 53, 48, 53, 48, 64, 316),
(516, 'Simipour', 'WATER', 75, 98, 63, 98, 63, 101, 498),
(519, 'Pidove', 'NORMAL, FLYING', 50, 55, 50, 36, 30, 43, 264),
(520, 'Tranquill', 'NORMAL, FLYING', 62, 77, 62, 50, 42, 65, 358),
(522, 'Blitzle', 'ELECTRIC', 45, 60, 32, 50, 32, 76, 295),
(523, 'Zwbstrika', 'ELECTRIC', 75, 100, 63, 80, 63, 116, 497),
(526, 'Gigalith', 'ROCK', 85, 135, 130, 60, 80, 25, 515),
(527, 'Woobat', 'Psychic, FLYING', 55, 45, 43, 55, 43, 72, 313),
(428, 'Swoobat', 'Psychic, FLYING', 67, 57, 55, 77, 55, 114, 425),
(529, 'Drilbur', 'GROUND', 60, 85, 40, 30, 45, 68, 328),
(530, 'Excadrill', 'GROUND, STEEL', 110, 135, 60, 50, 65, 88, 508),
(531, 'Audino', 'NORMAL', 103, 60, 86, 60, 86, 50, 445),
(531, 'Mega Audino', 'NORMAL, FAIRY', 103, 60, 126, 80, 126, 50, 545),
(532, 'Timburr', 'FIGHT', 75, 80, 55, 25, 35, 35, 305),
(533, 'Gurdurr', 'FIGHT', 85, 105, 85, 40, 50, 40, 405),
(534, 'Conkeldurr', 'FIGHT', 105, 140, 95, 55, 65, 45, 505),
(538, 'Throh', 'FIGHT', 120, 100, 85, 30, 85, 45, 645),
(539, 'Shawk', 'FIGHT', 75, 125, 75, 30, 75, 85, 465),
(540, 'Sewaddle', 'BUG, GRASS', 45, 53, 70, 40, 60, 42, 310),
(541, 'Swadloon', 'BUG, GRASS', 55, 63, 90, 50, 80, 42, 380),
(543, 'Leavanny', 'BUG, GRASS', 75, 103, 80, 70, 80, 92, 500),
(546, 'Cottonee', 'GRASS, FAIRY', 40, 27, 60, 37, 50, 66, 280),
(547, 'Whimisicott', 'GRASS, FAIRY', 60, 67, 85, 77, 75, 116, 480),
(551, 'Sandile', 'GROUND, DARK', 50, 72, 35, 35, 35, 65, 292),
(552, 'Krokorok', 'GROUND, DARK', 60, 82, 45, 45, 45, 74, 351),
(553, 'Krookodile', 'GROUND, DARK', 95, 117, 80, 65, 70, 92, 519),
(554, 'Darumaka', 'FIRE', 70, 90, 45, 15, 45, 50, 315),
(555, 'Darmanitan', 'FIRE', 105, 140, 55, 30, 55, 95, 480),
(559, 'Scraggy', 'DARK, FIGHT', 50, 75, 70, 35, 70, 48, 348),
(560, 'Scrafty', 'DARK, FIGHT', 65, 90, 115, 45, 115, 58, 488),
(561, 'Sigilyph', 'PCYHIC, FLYING', 72, 58, 80, 103, 80, 97, 490),
(566, 'Archen', 'ROCK, FLYING', 55, 112, 45, 74, 45, 70, 401),
(567, 'Archeops', 'ROCK, FLYING', 75, 140, 65, 112, 65, 110, 567),
(569, 'Garbodor', 'POISION', 80, 95, 82, 60, 82, 75, 474),
(570, 'Zorua', 'DARK', 40, 65, 40, 80, 40, 65, 330),
(571, 'Zoroark', 'DARK', 60, 105, 60, 120, 60, 105, 510),
(572, 'Minccino', 'NORMAL', 55, 50, 40, 40, 40, 75, 300),
(573, 'Cinccino', 'NORMAL', 75, 95, 60, 65, 60, 115, 470),
(577, 'Solosis', 'PSYCHIC', 45, 30, 40, 105, 50, 20, 290),
(578, 'Duosion', 'PSYCHIC', 65, 40, 50, 125, 60, 30, 370),
(579, 'Reuniclus', 'PSYCHIC', 110, 65, 75, 125, 85, 30, 490),
(580, 'Ducklett', 'WATER, FLYING', 62, 44, 50, 44, 50, 55, 305),
(581, 'Swanna', 'WATER, FLYING', 75, 87, 63, 87, 63, 98, 473),
(585, 'Deerling', 'NORMAL, GRASS', 60, 60, 50, 40, 50, 75, 335),
(586, 'Sawsbuck', 'NORMAL, GRASS', 80, 100, 70, 60, 70, 95, 475),
(588, 'Karrablast', 'BUG', 50, 75, 45, 40, 45, 60, 315),
(589, 'Escavalier', 'BUG, STEEL', 70, 135, 105, 60, 105, 20, 495),
(590, 'Foongus', 'GRASS, POISION', 69, 55, 45, 55, 55, 15, 294),
(591, 'Amoonguss', 'GRASS , POISION', 114, 85, 70, 85, 80, 30, 464),
(594, 'Alomomola', 'WATER', 165, 75, 80, 40, 45, 65, 470),
(595, 'Joltik', 'BUG, ELECTRIC', 50, 47, 50, 57, 50, 65, 319),
(596, 'Galvantula', 'BUG, ELECTRIC', 70, 77, 60, 97, 60, 108, 472),
(599, 'Klink', 'STEEl', 40, 55, 91, 24, 86, 10, 305),
(600, 'Klang', 'STEEL', 60, 80, 95, 70, 85, 50, 440),
(601, 'Klinklang', 'STEEL', 60, 100, 115, 70, 85, 90, 520),
(602, 'Tynamo', 'ELECTRIC', 35, 50, 40, 45, 40, 60, 275),
(603, 'Eelektrik', 'ELECTRIC', 65, 85, 70, 75, 70, 40, 405),
(605, 'Elgyem', 'PSYCHIC', 55, 55, 55, 85, 55, 30, 515),
(606, 'Beheeyem', 'PSYCHIC', 75, 75, 75, 125, 95, 40, 485),
(607, 'Litwick', 'GHOST, FIRE', 50, 30, 55, 65, 55, 20, 275),
(608, 'Lampent', 'GHOST, FIRE', 60, 40, 60, 95, 60, 55, 370),
(609, 'Chandelure', 'GHOST, FIRE', 60, 55, 90, 145, 90, 80, 520),
(610, 'Axew', 'DRAGOn', 46, 87, 60, 30, 40, 57, 320),
(611, 'Fraxure', 'DRAGON', 66, 117, 70, 40, 50, 67, 410),
(612, 'Haxorus', 'DRAGON', 76, 147, 90, 60, 70, 97, 540),
(613, 'Cubchoo', 'ICE', 55, 70, 40, 60, 40, 40, 305),
(614, 'Beartic', 'ICE', 95, 110, 80, 70, 80, 50, 485),
(615, 'Cryogonal', 'ICE', 70, 50, 30, 95, 135, 105, 485),
(616, 'Shelmet', 'BUG', 50, 40, 85, 40, 65, 25, 305),
(617, 'Accelgor', 'BUG', 80, 70, 40, 100, 60, 145, 495),
(618, 'Stunfisk', 'GROUND, ELECTRIC', 109, 66, 84, 81, 99, 32, 471),
(619, 'Mienfoo', 'FIGHT', 45, 85, 50, 55, 50, 65, 350),
(620, 'Mienshao', 'FIGHT', 65, 125, 60, 95, 60, 105, 510),
(621, 'Druddigon', 'DRAGON', 77, 120, 90, 60, 90, 48, 485),
(622, 'Golett', 'GROUND, GHOST', 59, 74, 50, 35, 50, 35, 303),
(623, 'Golurk', 'GROUND, GHOST', 89, 124, 80, 55, 80, 55, 483),
(624, 'Pawniard', 'DARK, STEEL', 45, 85, 70, 40, 40, 60, 340),
(625, 'Bisharp', 'DARK, STEEL', 65, 125, 100, 60, 70, 70, 490),
(426, 'Bouffalant', 'NORMAL', 95, 110, 95, 40, 95, 55, 490),
(627, 'Rufflet', 'NORMAL, FLYING', 70, 83, 50, 37, 50, 60, 350);



/* Strength Isertion */
INSERT INTO strength VALUES('FIGHTING', 'NORMAL');
INSERT INTO strength VALUES('FIGHTING', 'ROCK');
INSERT INTO strength VALUES('FIGHTING', 'STEEL');
INSERT INTO strength VALUES('FIGHTING', 'ICE');
INSERT INTO strength VALUES('FIGHTING', 'DARK');
INSERT INTO strength VALUES('FLYING', 'FIGHTING');
INSERT INTO strength VALUES('FLYING', 'BUG');
INSERT INTO strength VALUES('FLYING', 'GRASS');
INSERT INTO strength VALUES('POISON', 'GRASS');
INSERT INTO strength VALUES('POISON', 'FAIRY');
INSERT INTO strength VALUES('GROUND', 'POISON');
INSERT INTO strength VALUES('GROUND', 'ROCK');
INSERT INTO strength VALUES('GROUND', 'STEEL');
INSERT INTO strength VALUES('GROUND', 'FIRE');
INSERT INTO strength VALUES('GROUND', 'ELECTRIC');
INSERT INTO strength VALUES('ROCK', 'FLYING');
INSERT INTO strength VALUES('ROCK', 'BUG');
INSERT INTO strength VALUES('ROCK', 'FIRE');
INSERT INTO strength VALUES('ROCK', 'ICE');
INSERT INTO strength VALUES('BUG', 'GRASS');
INSERT INTO strength VALUES('BUG', 'PSYCHIC');
INSERT INTO strength VALUES('BUG', 'DARK');
INSERT INTO strength VALUES('GHOST', 'GHOST');
INSERT INTO strength VALUES('GHOST', 'PSYCHIC');
INSERT INTO strength VALUES('STEEL', 'ROCK');
INSERT INTO strength VALUES('STEEL', 'ICE');
INSERT INTO strength VALUES('STEEL', 'FAIRY');
INSERT INTO strength VALUES('FIRE', 'BUG');
INSERT INTO strength VALUES('FIRE', 'STEEL');
INSERT INTO strength VALUES('FIRE', 'GRASS');
INSERT INTO strength VALUES('FIRE', 'ICE');
INSERT INTO strength VALUES('WATER', 'GROUND');
INSERT INTO strength VALUES('WATER', 'ROCK');
INSERT INTO strength VALUES('WATER', 'FIRE');
INSERT INTO strength VALUES('GRASS', 'GROUND');
INSERT INTO strength VALUES('GRASS', 'ROCK');
INSERT INTO strength VALUES('GRASS', 'WATER');
INSERT INTO strength VALUES('ELECTRIC', 'FLYING');
INSERT INTO strength VALUES('ELECTRIC', 'WATER');
INSERT INTO strength VALUES('PSYCHIC', 'FIGHTING');
INSERT INTO strength VALUES('PSYCHIC', 'POISON');
INSERT INTO strength VALUES('ICE', 'FLYING');
INSERT INTO strength VALUES('ICE', 'GROUND');
INSERT INTO strength VALUES('ICE', 'GRASS');
INSERT INTO strength VALUES('ICE', 'DRAGON');
INSERT INTO strength VALUES('DRAGON', 'DRAGON');
INSERT INTO strength VALUES('FAIRY', 'FIGHTING');
INSERT INTO strength VALUES('FAIRY', 'DRAGON');
INSERT INTO strength VALUES('FAIRY', 'DARK');
INSERT INTO strength VALUES('DARK', 'GHOST');
INSERT INTO strength VALUES('DARK', 'PSYCHIC');

/* Resistance Inserion */
INSERT INTO resistance VALUES('NORMAL', 'GHOST');
INSERT INTO resistance VALUES('FIGHTING', 'ROCK');
INSERT INTO resistance VALUES('FIGHTING', 'BUG');
INSERT INTO resistance VALUES('FIGHTING', 'DARK');
INSERT INTO resistance VALUES('FLYING', 'FIGHTING');
INSERT INTO resistance VALUES('FLYING', 'GROUND');
INSERT INTO resistance VALUES('FLYING', 'BUG');
INSERT INTO resistance VALUES('FLYING', 'GRASS');
INSERT INTO resistance VALUES('POISON', 'FIGHTING');
INSERT INTO resistance VALUES('POISON', 'POISON');
INSERT INTO resistance VALUES('POISON', 'GRASS');
INSERT INTO resistance VALUES('POISON', 'FAIRY');
INSERT INTO resistance VALUES('GROUND', 'POISON');
INSERT INTO resistance VALUES('GROUND', 'ROCK');
INSERT INTO resistance VALUES('GROUND', 'ELECTRIC');
INSERT INTO resistance VALUES('ROCK', 'NORMAL');
INSERT INTO resistance VALUES('ROCK', 'FLYING');
INSERT INTO resistance VALUES('ROCK', 'POISON');
INSERT INTO resistance VALUES('ROCK', 'FIRE');
INSERT INTO resistance VALUES('BUG', 'FIGHTING');
INSERT INTO resistance VALUES('BUG', 'GROUND');
INSERT INTO resistance VALUES('BUG', 'GRASS');
INSERT INTO resistance VALUES('GHOST', 'NORMAL');
INSERT INTO resistance VALUES('GHOST', 'FIGHTING');
INSERT INTO resistance VALUES('GHOST', 'POISON');
INSERT INTO resistance VALUES('GHOST', 'BUG');
INSERT INTO resistance VALUES('STEEL', 'NORMAL');
INSERT INTO resistance VALUES('STEEL', 'FLYING');
INSERT INTO resistance VALUES('STEEL', 'POISON');
INSERT INTO resistance VALUES('STEEL', 'ROCK');
INSERT INTO resistance VALUES('STEEL', 'BUG');
INSERT INTO resistance VALUES('STEEL', 'STEEL');
INSERT INTO resistance VALUES('STEEL', 'GRASS');
INSERT INTO resistance VALUES('STEEL', 'PSYCHIC');
INSERT INTO resistance VALUES('STEEL', 'ICE');
INSERT INTO resistance VALUES('STEEL', 'DRAGON');
INSERT INTO resistance VALUES('STEEL', 'FAIRY');
INSERT INTO resistance VALUES('FIRE', 'BUG');
INSERT INTO resistance VALUES('FIRE', 'STEEL');
INSERT INTO resistance VALUES('FIRE', 'FIRE');
INSERT INTO resistance VALUES('FIRE', 'GRASS');
INSERT INTO resistance VALUES('FIRE', 'ICE');
INSERT INTO resistance VALUES('WATER', 'STEEL');
INSERT INTO resistance VALUES('WATER', 'FIRE');
INSERT INTO resistance VALUES('WATER', 'WATER');
INSERT INTO resistance VALUES('WATER', 'ICE');
INSERT INTO resistance VALUES('GRASS', 'GROUND');
INSERT INTO resistance VALUES('GRASS', 'WATER');
INSERT INTO resistance VALUES('GRASS', 'GRASS');
INSERT INTO resistance VALUES('GRASS', 'ELECTRIC');
INSERT INTO resistance VALUES('ELECTRIC', 'FLYING');
INSERT INTO resistance VALUES('ELECTRIC', 'STEEL');
INSERT INTO resistance VALUES('ELECTRIC', 'ELECTRIC');
INSERT INTO resistance VALUES('PSYCHIC', 'FIGHTING');
INSERT INTO resistance VALUES('PSYCHIC', 'PSYCHIC');
INSERT INTO resistance VALUES('ICE', 'ICE');
INSERT INTO resistance VALUES('DRAGON', 'FIRE');
INSERT INTO resistance VALUES('DRAGON', 'WATER');
INSERT INTO resistance VALUES('DRAGON', 'GRASS');
INSERT INTO resistance VALUES('DRAGON', 'ELECTRIC');
INSERT INTO resistance VALUES('FAIRY', 'FIGHTING');
INSERT INTO resistance VALUES('FAIRY', 'BUG');
INSERT INTO resistance VALUES('FAIRY', 'DRAGON');
INSERT INTO resistance VALUES('FAIRY', 'DARK');
INSERT INTO resistance VALUES('DARK', 'GHOST');
INSERT INTO resistance VALUES('DARK', 'PSYCHIC');
INSERT INTO resistance VALUES('DARK', 'DARK');


INSERT INTO form VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/220/215/199/413.png', 1, 413, 'Wormadam (Sandy Cloak)', 'BUG', 'GROUND', 60, 79, 105, 59, 85, 36, 424);
INSERT INTO form VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/220/215/199/413.png', 2, 413, 'Wormadam (Trash Cloak)', 'BUG', 'STEEL', 60, 69, 95, 69, 95, 36, 424);
INSERT INTO form VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/228/215/199/421.png', 1, 421, 'Cherrim (Sunshine Form)', 'GRASS', NULL, 70, 60, 70, 87, 78, 85, 450);
INSERT INTO form VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/286/215/199/479.png', 1, 479, 'Rotom (Heat)', 'ELECTRIC', 'FIRE', 50, 65, 107, 105, 107, 86, 520);
INSERT INTO form VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/286/215/199/479.png', 2, 479, 'Rotom (Wash)', 'ELECTRIC', 'WATER', 50, 65, 107, 105, 107, 86, 520);
INSERT INTO form VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/286/215/199/479.png', 3, 479, 'Rotom (Frost)', 'ELECTRIC', 'ICE', 50, 65, 107, 105, 107, 86, 520);
INSERT INTO form VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/286/215/199/479.png', 4, 479, 'Rotom (Fan)', 'ELECTRIC', 'FLYING', 50, 65, 107, 105, 107, 86, 520);
INSERT INTO form VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/286/215/199/479.png', 5, 479, 'Rotom (Mow)', 'ELECTRIC', 'GRASS', 50, 65, 107, 105, 107, 86, 520);
INSERT INTO form VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/294/215/199/487.png', 1, 487, 'Giratina (Origin)', 'GHOST', 'DRAGON', 150, 120, 100, 120, 100, 90, 680);
INSERT INTO form VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/299/215/199/492.png', 1, 492, 'Shaymin (Sky Form)', 'GRASS', 'FLYING', 100, 103, 75, 120, 75, 127, 600);
INSERT INTO form VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/357/215/199/550.png', 1, 550, 'Basculin (Blue Form)', 'WATER', NULL, 70, 92, 65, 80, 55, 98, 460);
INSERT INTO form VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/362/215/199/555.png', 1, 555, 'Darmanitan (Zen Mode)', 'FIRE', 'PSYCHIC', 105, 30, 105, 140, 105, 55, 540);

INSERT INTO type VALUES('NORMAL');
INSERT INTO type VALUES('FIGHTING');
INSERT INTO type VALUES('FLYING');
INSERT INTO type VALUES('POISON');
INSERT INTO type VALUES('GROUND');
INSERT INTO type VALUES('ROCK');
INSERT INTO type VALUES('BUG');
INSERT INTO type VALUES('GHOST');
INSERT INTO type VALUES('STEEL');
INSERT INTO type VALUES('FIRE');
INSERT INTO type VALUES('WATER');
INSERT INTO type VALUES('GRASS');
INSERT INTO type VALUES('ELECTRIC');
INSERT INTO type VALUES('PSYCHIC');
INSERT INTO type VALUES('ICE');
INSERT INTO type VALUES('DRAGON');
INSERT INTO type VALUES('FAIRY');
INSERT INTO type VALUES('DARK');

INSERT INTO pokemonvulnerable VALUES('FIGHTING', 'NORMAL');
INSERT INTO pokemonvulnerable VALUES('FIGHTING', 'ROCK');
INSERT INTO pokemonvulnerable VALUES('FIGHTING', 'STEEL');
INSERT INTO pokemonvulnerable VALUES('FIGHTING', 'ICE');
INSERT INTO pokemonvulnerable VALUES('FIGHTING', 'DARK');
INSERT INTO pokemonvulnerable VALUES('FLYING', 'FIGHTING');
INSERT INTO pokemonvulnerable VALUES('FLYING', 'BUG');