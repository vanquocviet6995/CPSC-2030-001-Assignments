<!DOCTYPE html>
<html style="height: 100%;">
 {% include('head.twig.html') with {'title':title} %}
 


<body style="height: 100%;">
{% include('top.twig.html') %}
{% include('nav.twig.php') %}


 <?php
session_start();

if(!isset($_SESSION['user']))
{
  header("Location: index.php");
}
if(isset($_GET['logout']))
{
  session_destroy();
  unset($_SESSION['user']);
  header("Location: index.php");
}

?>

<!-- END OF NAVIGATION -->


<!-- BODY -->
	<div class="section sectone"><div style="font-size: 50px; color: white; padding-top: 80px; margin-left: 43%;">About Us
      
      
     </div></div>
	<div class="subsection1" >
  <div class="row" >
<div class="col-sm-5">

    <img src="images/177515-004-3EAA8C4B.jpg"  height="400px" width="350px" align="left" >

    </div>
    <div class="col-sm-7">
    <h1  style="padding-top: 150px;">World War One</h1>
    <p style="margin-top: 30px; margin-right: 60px; margin-left: 30px;">World War I , otherwise called the First World War or the Great War, was a worldwide war beginning in Europe that kept going from 28 July 1914 to 11 November 1918. Contemporaneously depicted as the "war to end all wars",it prompted the activation of in excess of 70 million military work force, including 60 million Europeans, making it one of the biggest wars in history.</p>
  </div>
</div>
</div>
<br /> 
<br />
<!-- 	<div class="section secttwo"></div>
 -->	<div class="subsection2">
		<h1>FIVE THINGS YOU NEED TO KNOW ABOUT THE FIRST WORLD WAR</h1>
		<div class="gallery">
  <a target="_blank" href="images/418067.jpg">
    <img src="images/one.jpg" alt="5Terre" >
  </a>
  <div class="desc">IT WAS A GLOBAL WAR</div>
  <p>More than 30 countries pronounced war somewhere in the range of 1914 and 1918. The dominant part joined in favor of the Allies, including Serbia, Russia, France, Britain, Italy and the United States. </p>
</div>

<div class="gallery">
  <a target="_blank" href="images/418067.jpg">
    <img src="images/418067.jpg" alt="Forest">
  </a>
  <div class="desc">IT IS FAR BETTER TO FACE THE BULLETS...</div>
  <br />
  <p>The First World War was not inescapable or inadvertent, but rather started because of human activities . More than 65 million men volunteered or were recruited to battle in mass native armed forces.  </p>
</div>
<br />
<div class="gallery">
  <a target="_blank" href="images/418067.jpg">
    <img src="images/three.jpg" alt="Northern Lights" width="300px" height="400">
  </a>
  <div class="desc">IT WAS A WAR OF PRODUCTION</div>
  <p>National assets were activated as every soldier country hustled to supply its military with enough men.In Britain, early disappointments in weapons fabricating prompted full government intercession in war creation.</p>
</div>
<div class="gallery">
  <a target="_blank" href="images/418067.jpg">
    <img src="images/four.jpg" alt="Northern Lights" width="300px" height="400">
  </a>
  <div class="desc">IT WAS A WAR OF INNOVATION</div>
  <p>Advances in weaponry and military innovation incited strategic changes as each side endeavored to pick up leverage over the other. The presentation of airplane into war left troopers and regular folks.</p>
</div>
<div class="gallery">
  <a target="_blank" href="images/418067.jpg">
    <img src="images/five.jpg" alt="Northern Lights" width="300px" height="400">
  </a>
  <div class="desc">IT WAS A WAR OF DESTRUCTION</div>
  <p>The First World War left an expected 16 million fighters and regular people dead and mentally injured. The war additionally everlastingly adjusted the world's social and political scene.</p>
</div>
  </div>
  <br />
  <br />
  
<!-- 	<div class="section sectthree"></div>
 -->	<div class="subsection3">
	<h1>OPERATION OVERLORD – D-DAY</h1>
	<img src="images/ww21.jpg" align="left">
	<p >Operation Overlord was the code name for the Battle of Normandy, the Allied operation that launched the successful invasion of German-occupied western Europe <br>during World War II. The operation commenced on 6 June 1944 with the Normandy landings (commonly known as D-Day). 156,000 British, US and <br>Canadian troops landed in northern France and broke through German defences. It was the largest amphibious attack from water to land in history.<br>More than 370,000 Allied troops were involved in the Battle of Normandy, with over 2,500 casualties being recorded on the first day alone.This successful Allied <br>operation marked the beginning of the end of the war and of Nazi control of Germany.</p>
	
  </div>
  <br />
  <br />
  <br />
  <br />
  <br />

<!-- END OF BODY -->




<!-- FOOTER -->

{% include('footer.twig.html')%}
	
<!-- END OF FOOTER -->


</body>
</html>