<?php 

class Response {

	public function __construct(){}

	public function send($data, $status = null){
			$this->status($status);
			
			echo $data;
	}
	
	public function json($data, $status = null) {
		$this->status($status);

		header('Content-Type: application/json');

		echo json_encode($data);
	}

	public function sendFile($path, $status = null) {
		$this->status($status);
		echo file_get_contents($path);			
	}
	
	public static function status($status){
		if (isset($status)) {
			http_response_code($status);
			return 1;
		}

		return 0;
	}
}