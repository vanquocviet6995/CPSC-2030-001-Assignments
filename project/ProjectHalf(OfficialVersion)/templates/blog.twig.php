<!DOCTYPE html>
<html lang="en">
{% include('head.twig.html') with {'title':title} %}
<style>
 

@media (min-width: 992px) 

</style>
  <body>
 <?php
session_start();

if(!isset($_SESSION['user']))
{
  header("Location: index.php");
}
if(isset($_GET['logout']))
{
  session_destroy();
  unset($_SESSION['user']);
  header("Location: index.php");
}

?>
   <!-- NAVIGATION -->




<body style="height: 100%;">
{% include('top.twig.html') %}
{% include('nav.twig.php') %}
 
    
<!-- END OF NAVIGATION -->

    <!-- Page Content -->
    <div class="container">

      <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8" style="padding-top: 30px;"> 

          

          <!-- Blog Post -->
          <div class="card mb-4">
            <img class="card-img-top" src="images/aa.jpg" alt="Card image cap" height="400px">
            <div class="card-body">
            
              <p class="card-text">Why Central Americans join the caravan: ‘because there’s no scarier place than home’</p>
              <a href="https://www.rescue.org/article/why-central-americans-join-caravan-because-theres-no-scarier-place-home" class="btn btn-primary">Read More </a>
            </div>
         
          </div>

          <!-- Blog Post -->
          <div class="card mb-4">
            <img class="card-img-top" src="images/aaa.jpg" alt="Card image cap" height="400px">
            <div class="card-body">
           
              <p class="card-text">Little girl braves cancer amid the war in Yemen</p>
              <a href="read1.html" class="btn btn-primary">Read More </a>
            </div>
          
          </div>

          <!-- Blog Post -->
        
        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">

          <!-- Search Widget -->
          <div class="card my-4">
            <h5 class="card-header">Search</h5>
            <div class="card-body">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
                  <button class="btn btn-secondary" type="button">Go!</button>
                </span>
              </div>
            </div>
          </div>

          <!-- Categories Widget -->
          <div class="card my-4">
            <h5 class="card-header">Other</h5>
            <div class="card-body">
              <div class="row">
                <div class="col-lg-6">
                  <ul class="list-unstyled mb-0">
                    
                    <li>
                      <a href="https://www.rescue.org/article/elite-athlete-and-refugee-set-run-his-first-new-york-city-marathon">Elite athlete</a>
                    </li>
                    <li>
                      <a href="https://www.rescue.org/country/yemen">Yemen crisis</a>
                    </li>
                  </ul>
                </div>
                <div class="col-lg-6">
                  <ul class="list-unstyled mb-0">
                    <li>
                      <a href="https://www.rescue.org/article/real-stories-central-american-families-searching-safety">American families </a>
                    </li>
                    <li>
                      <a href="https://www.rescue.org/article/families-seeking-asylum-violence-central-america-are-not-criminals">Families seeking</a>
                    </li>
                    
                  </ul>
                </div>
              </div>
            </div>
          </div>

   

        </div>

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->


{% include('footer.twig.html')%}
  </body>

</html>
