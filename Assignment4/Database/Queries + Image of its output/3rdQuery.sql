/* 3. First query for pokemon order by speed */

SELECT p.name
FROM pokemon p
INNER JOIN resistance r
ON p.type = r.weak
WHERE r.resistant = "WATER" AND p.bst >= 200 AND p.bst <= 500;