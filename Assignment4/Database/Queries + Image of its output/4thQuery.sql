/* 4.	Find the Pokémon with the highest Atk, has a Mega evolution form and vulnerable to fire */

SELECT `Name`, `Type`, `HP`, MAX(Atk), `Def`, `SAt`, `SDf`, `Spd`, `BST`, `Strong`, `Weak`, `Resistant`, `Vulnerable` FROM mega WHERE `Vulnerable` = "fire"