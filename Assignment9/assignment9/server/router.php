<?php 

require_once "response.php";

class Router {
	
	private $routes = [];
	private $method = null;
	private $currentPath = null;
	
	
	private $errorFunction = null;
	private $request = null;
	private $response = null;
	
	public function __construct() {
		
			if (isset($_SERVER['REQUEST_METHOD'])) {
					$this->method = $_SERVER['REQUEST_METHOD'];
					$this->request["method"] = $_SERVER['REQUEST_METHOD'];
			}
	
			$url = parse_url($_SERVER["REQUEST_URI"]);
						
			$this->currentPath = $url["path"];
			
			if (isset($_POST))
					$this->request["body"] = $_POST;

			if (isset($_GET))
					$this->request["params"] = $_GET;

			$this->response = new Response();
			$this->routes = [
				'GET' => [],
				'POST' => []
			];
	}

	public function get($path, $callback) {
		$this->routes['GET'][$path] = $callback;
	}

	public function post($path, $callback) {
		$this->routes['POST'][$path] = $callback;
	}

	public function error($function) {
		$this->errorFunction = $function;
	}

	
	public function getCallback($method) {
			if (!isset($this->routes[$method]))
				return null;

			foreach ($this->routes[$method] as $name => $value) {
				if ($name === $this->currentPath || $name === $this->currentPath . '/') {
					return $this->routes[$method][$name];
				}
			}
	}

	public function start() {

			$callback = $this->getCallBack($this->method);

			if ($callback)
				return $callback($this->request, $this->response);

			if (isset($this->errorFunction))
				return ($this->errorFunction)(new Exception("Invalid path", 404), $this->response);

	}
}