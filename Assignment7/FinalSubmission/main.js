//var for battalionList array (all character information and image url)

var battalionList = [{
        Name: "Claude Wallace",
        Side: "Edinburgh Army",
        Unit: "Ranger Corps, Squad E",
        Rank: "First Lieutenant",
        Role: "Tank Commander",
        Description: "Born in the Gallian city of Hafen, this promising young squad leader is keenly attuned to climate changes and weather fluctuations. Ever since graduating as valedictorian from the Royal Military Academy, his talent and determination have been an inspiration to his subordinates.",
        ImageUrl: "http://valkyria.sega.com/img/character/chara01.png"
    },
    {
        Name: "Kai Schulen",
        Side: "Edinburgh Army",
        Unit: "Ranger Corps, Squad E",
        Rank: "Sergeant Major",
        Role: "Fireteam Leader",
        Description: "Born in the Gallian city of Hafen, this cool and collected sharpshooter has earned the codename Deadeye Kai. Along with her childhood friends, she joined a foreign military to take the fight to the Empire. She loves fresh-baked bread, almost to a fault.",
        ImageUrl: "http://valkyria.sega.com/img/character/chara04.png"
    },
    {
        Name: "Brian Haddock",
        Side: "Edinburgh Navy",
        Unit: "Centurion, Cygnus Fleet",
        Rank: "Lieutenant",
        Role: "Chief Navigator",
        Description: "As the Centurion's crewmember responsible for navigation, this straight-laced navy man is considered by his peers to be the ship's de facto first mate. He holds Cpt. Morgen in very high regard. Enjoys gardening, and keeps a collection of potted plants in his quarters.",
        ImageUrl: "http://valkyria.sega.com/img/character/chara19.png"
    },
    {
        Name: "Dan Bentley",
        Side: "Edinburgh Army",
        Unit: "Ranger Corps, Squad E",
        Rank: "Private First Class",
        Role: "APC Operator",
        Description: "Born in the United States of Vinland, this driver loves armored personnel carriers with a passion. His skill behind the wheel is matched only by his way with a wrench. Though not much of a talker, he takes pride in carrying his teammates through combat.",
        ImageUrl: "http://valkyria.sega.com/img/character/chara16.png"
    },
    {
        Name: "Roland Morgen",
        Side: "Edinburgh Army",
        Unit: "Ranger Corps, Squad E",
        Rank: "Sergeant",
        Role: "Fireteam Leader",
        Description: "Born in the Gallian city of Hafen, this foul-mouthed Darcsen worked his way up from the slums to become a capable soldier. Though foul-mouthed and reckless, his athleticism and combat prowess is top-notch... And according to him, he's invincible.",
        ImageUrl: "http://valkyria.sega.com/img/character/chara03.png"
    },
    {
        Name: "Marie Bennett",
        Side: "Edinburgh Navy",
        Unit: "Centurion, Cygnus Fleet",
        Rank: "Petty Officer",
        Role: "Chief of Operations",
        Description: "As the Centurion's crewmember responsible for overseeing daily operations, this gentle and supportive EWI veteran even takes daily tasks like cooking and cleaning upon herself. She never forgets to wear a smile. Her age is undisclosed, even in her personnel files.",
        ImageUrl: "http://valkyria.sega.com/img/character/chara20.png"
    },
    {
        Name: "Ronald Albee",
        Side: "Edinburgh Army",
        Unit: "Ranger Corps, Squad F",
        Rank: "Second Lieutenant",
        Role: "Tank Operator",
        Description: "Born in the United Kingdom of Edinburgh, this stern driver was Minerva Victor's underclassman at the military academy. Upon being assigned to Squad F, he swore an oath of fealty to Lt. Victor, and takes great satisfaction in upholding her chivalric code.",
        ImageUrl: "http://valkyria.sega.com/img/character/chara17.png"
    },
    {
        Name: "Miles Arbeck",
        Side: "Edinburgh Army",
        Unit: "Ranger Corps, Squad E",
        Rank: "Sergeant",
        Role: "Tank Operator",
        Description: "Born in the United Kingdom of Edinburgh, this excitable driver was Claude Wallace's partner in tank training, and was delighted to be assigned to Squad E. He's taken up photography as a hobby, and is constantly taking snapshots whenever on standby.",
        ImageUrl: "http://valkyria.sega.com/img/character/chara15.png"
    },
    {
        Name: "Ragnarok",
        Side: "Edinburgh Army",
        Unit: "Squad E",
        Rank: "K-9 Unit",
        Role: "Mascot",
        Description: "Once a stray, this good good boy is lovingly referred to as Rags. As a K-9 unit, he's a brave and intelligent rescue dog who's always willing to lend a helping paw. When the going gets tough, the tough get ruff.",
        ImageUrl: "http://valkyria.sega.com/img/character/chara13.png"
    },
    {
        Name: "Minerva Victor",
        Side: "Edinburgh Army",
        Unit: "Ranger Corps, Squad F",
        Rank: "First Lieutenant",
        Role: "Senior Commander",
        Description: "Born in the United Kingdom of Edinburgh to a noble family, this competitive perfectionist has authority over the 101st Division's squad leaders. She values honor and chivalry, though a bitter rivalry with Lt. Wallace sometimes compromises her lofty ideals.",
        ImageUrl: "http://valkyria.sega.com/img/character/chara11.png"
    }
];

// set up function loadData
function loadData() {

    Array.prototype.remove = function() {
        var what, a = arguments,
            L = a.length,
            ax;
        while (L && this.length) {
            what = a[--L];
            while ((ax = this.indexOf(what)) !== -1) {
                this.splice(ax, 1);
            }
        }
        return this;
    };

    var ul = document.getElementById("dynamic-list");

    var squadUl = document.getElementById("squad-list");
    var items = [];
    var flag = false;

    for (var i = 0; i < battalionList.length; i++) {
        var bl = battalionList[i]["Name"];
        var listItem = document.createElement("li");
        listItem.setAttribute("id", "test");
        listItem.textContent = bl;
        listItem.addEventListener("click", function() {


            //When the user click on a character in the army list, The appearance of the name should change to indicate that it's selected
            if (document.getElementsByClassName('highlight').length >= 1) {
                var current = document.getElementsByClassName("highlight");
                current[0].setAttribute("class", "");
            }
            this.setAttribute("class", "highlight");

            var squadListItem = document.createElement("li");

            if (!(document.getElementById("squad-list").getElementsByTagName("li").length >= 5)) {

                if (document.getElementById("squad-list").getElementsByTagName("li").length == 0) {
                    squadListItem.textContent = this.textContent;
                    squadUl.appendChild(squadListItem);
                }
                if (items.includes(this.textContent) == true && flag == false) {
                    console.log("items exists!!!");
                } else {
                    squadListItem.textContent = this.textContent;
                    squadUl.appendChild(squadListItem);

                    squadListItem.addEventListener("mouseover", function() {
                        var imageUrl;
                        var characterDescription;

                        for (var i = 0; i < battalionList.length; i++) {
                            if (battalionList[i]["Name"].match(squadListItem.textContent)) {
                                imageUrl = battalionList[i]["ImageUrl"];
                                characterDescription = battalionList[i];
                                break;
                            }
                        }

                        var elem = document.createElement("img");
                        elem.setAttribute("src", imageUrl);
                        elem.setAttribute("height", "400");
                        elem.setAttribute("width", "300");

                        document.getElementById("characterPicture").innerHTML = "";
                        document.getElementById("characterInformation").innerHTML = "";

                        document.getElementById("characterPicture").appendChild(elem);
                        document.getElementById("characterInformation").innerHTML += "<strong>Name:</strong> " + characterDescription["Name"] + "<br/><strong>Side:</strong> " + characterDescription["Side"] + "<br/><strong>Unit:</strong> " + characterDescription["Unit"] + "<br/><strong>Rank:</strong> " + characterDescription["Rank"] + "<br/><strong>Role:</strong> " + characterDescription["Role"] + "<br/><strong>Description:</strong> " + characterDescription["Description"] + "<br/><br/>";
                    });

                    squadListItem.addEventListener("mouseout", function() {
                        document.getElementById("characterPicture").innerHTML = "";
                        document.getElementById("characterInformation").innerHTML = "";
                    });

                    squadListItem.addEventListener("click", function() {
                        items.remove(this.textContent);
                        this.parentNode.removeChild(this);
                    });
                }
                items.push(this.textContent);
                flag = false;
            }
        });


        // Add EventListener 
        listItem.addEventListener("mouseover", function selectItem() {

            var imageUrl;
            var characterDescription;

            for (var i = 0; i < battalionList.length; i++) {
                if (battalionList[i]["Name"].match(this.textContent)) {
                    imageUrl = battalionList[i]["ImageUrl"];
                    characterDescription = battalionList[i];
                    break;
                }
            }
            
            // Display the output
            var elem = document.createElement("img");
            elem.setAttribute("src", imageUrl);
            elem.setAttribute("height", "400");
            elem.setAttribute("width", "300");

            document.getElementById("characterPicture").innerHTML = "";
            document.getElementById("characterInformation").innerHTML = "";

            document.getElementById("characterPicture").appendChild(elem);
            document.getElementById("characterInformation").innerHTML += "<strong>Name:</strong> " + characterDescription["Name"] + "<br/><strong>Side:</strong> " + characterDescription["Side"] + "<br/><strong>Unit:</strong> " + characterDescription["Unit"] + "<br/><strong>Rank:</strong> " + characterDescription["Rank"] + "<br/><strong>Role:</strong> " + characterDescription["Role"] + "<br/><strong>Description:</strong> " + characterDescription["Description"] + "<br/><br/>";

        });

        listItem.addEventListener("mouseout", function() {

            document.getElementById("characterPicture").innerHTML = "";
            document.getElementById("characterInformation").innerHTML = "";

        });

        ul.appendChild(listItem);
    }
}