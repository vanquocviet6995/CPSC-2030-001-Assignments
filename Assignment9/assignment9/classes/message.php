<?php

require(__DIR__ . '/../connection.php');

class Message {

	public $conn = null;

	public function __construct() {
		$this->conn = Database::getConnection();		
	}


	public function add($message) {

		$message["date"] = time();

		$stmt = $this->conn->prepare("INSERT INTO messages (user, text, date) VALUES (:user, :text, :date)");

	    if($stmt->execute($message))
	    	return $this->format($message);

	    return null;

	}

	private function format($message) {

		$message["time"] = date('H:i:s', $message["date"]);

		return $message;

	}

	public function fetch($date, $limit = 10) {

		$placeholders = ["limit" => $limit];

		
		$placeholders["date"] = $date;
		

		$stmt = $this->conn->prepare("SELECT * FROM messages WHERE date > :date ORDER BY ID DESC LIMIT :limit");

		$stmt->execute($placeholders);

		$messages = $stmt->fetchAll();

		return array_reverse(
			array_map(array($this, 'format'), $messages)
		);

	}

}